#/usr/bin/env python3
# _*_ coding: utf-8 _*_

import json
from pymappali import window, util

if __name__ == '__main__':
  # open conf file
  try:
    with open('settings.json', 'r') as f:
      conf = json.load(f)
  except IOError:
    util.error('settings.json file not present')
  
  # open save file
  try:
    with open(conf['data'], 'r') as f:
      db = json.load(f)
  except IOError:
    util.check_dir('data')
    db = {}
  
  win = window.Window(conf, db)
  win.exit()