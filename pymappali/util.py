#/usr/bin/env python3
# _*_ coding: utf-8 _*_

import os, errno, json
import subprocess as sp
import base64 as b64
from os import path
from PIL import Image
from io import BytesIO

def error(string):
  print('[x] {}.'.format(string), file=sys.stderr)

def save_db(conf, db):
  with open(conf['data'], 'w') as f:
    json.dump(db, f, indent=4)

def check_file(file):
  '''
  Check if file exists.
  
  < file: path of file.
  
  > true or false
  '''
  if path.exists(file):
    return True
  raise RuntimeError

def check_dir(path):
  '''
  Check if directory exists, otherwise create it.
  
  < path: path of directory.
  '''
  try:
    os.makedirs(path)
  except OSError as exception:
    if exception.errno != errno.EEXIST:
      raise

def b64_encode(path):
  if check_file(path):
    with open(path, 'rb') as image:
      return b64.b64encode(image.read())

def b64_display(enc_str, conf):
  '''
  Display an image from a base64 encoded image.
  
  < enc_str: base64 encoded image.
  < conf: config opbject
  '''
  if not enc_str:
    with open(conf['error_image'], 'rb') as image:
      enc_str = b64.b64encode(image.read())
  im = Image.open(BytesIO(b64.b64decode(enc_str)))
  im.show()

def display(path):
  '''
  Display an image.
  
  < path: path of an image.
  '''
  if check_file(path):
    im = Image.open(path)
    im.show()