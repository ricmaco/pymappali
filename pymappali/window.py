#/usr/bin/env python3
# _*_ coding: utf-8 _*_

import sys, os
from os import path
from PyQt5.QtWidgets import (
  QWidget,
  QApplication,
  QVBoxLayout,
  QHBoxLayout,
  QTabWidget,
  QFrame,
  QLabel,
  QLineEdit,
  QTextEdit,
  QPushButton,
  QFileDialog
)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QDir
from PyQt5.Qt import Qt

from pymappali import util

class Window(QWidget):

  def __init__(self, conf, db):
    self.conf = conf
    self.db = db
    self.app = QApplication(sys.argv)
    
    super().__init__()
    self.setWindowTitle('PyMappali')
    self.setWindowIcon(QIcon(conf['icon']))
    
    central = QVBoxLayout()
    self.setLayout(central)
    
    self.message = QLabel()
    self.message.setStyleSheet(
      'QLabel { padding: 3px; background-color: red; color: white; }')
    self.message.hide()
    central.addWidget(self.message)
    
    tab = QTabWidget()
    self.insert = QFrame()
    self.search = QFrame()
    tab.addTab(self.insert, 'Inserisci mappale')
    tab.addTab(self.search, 'Ricerca mappale')
    central.addWidget(tab)
    
    self.ivbox = QVBoxLayout()
    self.ivbox.setAlignment(Qt.AlignTop)
    self.insert.setLayout(self.ivbox)
    
    self.svbox = QVBoxLayout()
    self.svbox.setAlignment(Qt.AlignTop)
    self.search.setLayout(self.svbox)
    
    self._insert_tab()
    self._search_tab()
    
    self.setMinimumWidth(250)  
    self.show()
  
  def _insert_tab(self):
    l_name = QLabel('Nome:')
    self.ivbox.addWidget(l_name)
    
    self.t_name = QLineEdit()
    self.t_name.insert('Francesco')
    self.ivbox.addWidget(self.t_name)
    
    l_surname = QLabel('Cognome:')
    self.ivbox.addWidget(l_surname)
    
    self.t_surname = QLineEdit()
    self.t_surname.insert('Bullio')
    self.ivbox.addWidget(self.t_surname)
    
    l_city = QLabel('Comune:')
    self.ivbox.addWidget(l_city)
    
    self.t_city = QLineEdit()
    self.t_city.insert('Somma Lombardo')
    self.ivbox.addWidget(self.t_city)
    
    l_paper = QLabel('Foglio:')
    self.ivbox.addWidget(l_paper)
    
    self.t_paper = QLineEdit()
    self.ivbox.addWidget(self.t_paper)
    
    l_map = QLabel('Mappale:')
    self.ivbox.addWidget(l_map)
    
    self.t_map = QLineEdit()
    self.ivbox.addWidget(self.t_map)
    
    l_area = QLabel('Superficie:')
    self.ivbox.addWidget(l_area)
    
    self.t_area = QLineEdit()
    self.ivbox.addWidget(self.t_area)
    
    self.images = {}
    l_images = QLabel('Immagini:')
    self.ivbox.addWidget(l_images)
    
    b_images = QPushButton('Inserisci immagine...')
    self.ivbox.addWidget(b_images)
    b_images.pressed.connect(self._show_dialog)
    
    self.im_vbox = QVBoxLayout()
    self.ivbox.addLayout(self.im_vbox)
    
    separator = QLabel()
    separator.setStyleSheet('QFrame { margin-top: 10px; }')
    self.ivbox.addWidget(separator)
    
    b_ok = QPushButton('Inserisci')
    b_ok.setStyleSheet(
      'QPushButton { font-size: 20px; font-weight: bold; padding: 5px; }')
    b_ok.pressed.connect(self._write)
    self.ivbox.addWidget(b_ok)
  
  def _search_tab(self):
    l_search = QLabel('Inserisci il numero di mappale:')
    self.svbox.addWidget(l_search)
    
    self.t_search = QLineEdit()
    self.svbox.addWidget(self.t_search)
    
    b_search = QPushButton('Cerca')
    b_search.pressed.connect(self._search)
    self.svbox.addWidget(b_search)
    
    self.results = QFrame()
    self.results.hide()
    self.svbox.addWidget(self.results)
    
    self.res_vbox = QVBoxLayout()
    self.res_vbox.setAlignment(Qt.AlignTop)
    self.results.setLayout(self.res_vbox)
    
    l_name = QLabel('Nome:')
    self.res_vbox.addWidget(l_name)
    
    self.r_name = QLineEdit()
    self.r_name.setReadOnly(True)
    self.res_vbox.addWidget(self.r_name)
    
    l_surname = QLabel('Cognome:')
    self.res_vbox.addWidget(l_surname)
    
    self.r_surname = QLineEdit()
    self.r_surname.setReadOnly(True)
    self.res_vbox.addWidget(self.r_surname)
    
    l_city = QLabel('Comune:')
    self.res_vbox.addWidget(l_city)
    
    self.r_city = QLineEdit()
    self.r_city.setReadOnly(True)
    self.res_vbox.addWidget(self.r_city)
    
    l_paper = QLabel('Foglio:')
    self.res_vbox.addWidget(l_paper)
    
    self.r_paper = QLineEdit()
    self.r_paper.setReadOnly(True)
    self.res_vbox.addWidget(self.r_paper)
    
    l_map = QLabel('Mappale:')
    self.res_vbox.addWidget(l_map)
    
    self.r_map = QLineEdit()
    self.r_map.setReadOnly(True)
    self.res_vbox.addWidget(self.r_map)
    
    l_area = QLabel('Superficie:')
    self.res_vbox.addWidget(l_area)
    
    self.r_area = QLineEdit()
    self.r_area.setReadOnly(True)
    self.res_vbox.addWidget(self.r_area)
    
    l_images = QLabel('Immagini:')
    self.res_vbox.addWidget(l_images)
  
  def _show_dialog(self):
    filename = QFileDialog.getOpenFileName(self, 'Apri immagine',
      QDir.homePath(), 'File immagine (*.jpg *.jpeg *.png *.bmp)')
    
    if filename[0]:
      basename = path.basename(filename[0])
      
      # create horizontal container
      im_hbox = QHBoxLayout()
      self.im_vbox.addLayout(im_hbox)
      
      b_tmp = QPushButton('Apri')
      b_tmp.pressed.connect(lambda: util.display(filename[0]))
      im_hbox.addWidget(b_tmp)
      
      l_tmp = QLabel(basename)
      im_hbox.addWidget(l_tmp)
      d_tmp = QPushButton('Cancella')
      
      def _delete_image(*args):
        self._delete(*args)
        del self.images[(b_tmp, l_tmp, d_tmp)]
      d_tmp.pressed.connect(lambda: _delete_image(b_tmp, l_tmp, d_tmp))
      im_hbox.addWidget(d_tmp)
      
      # save image in b64
      self.images[(b_tmp, l_tmp, d_tmp)] = util.b64_encode(filename[0])
  
  def _search(self):
    key = self.t_search.text()
    s_key = key.strip()
    if key != s_key:
      key = s_key
      self.t_search.setText(s_key)
      del s_key
    if key:
      try:
        images = []
        result = self.db[key]
        # pulisci
        self._flash('', '{}', True)
        self._delete(*images)
        self.images.clear()
        self.results.show()
        # populate results
        self.r_name.setText(result['nome'])
        self.r_surname.setText(result['cognome'])
        self.r_city.setText(result['comune'])
        self.r_paper.setText(result['foglio'])
        self.r_map.setText(result['mappale'])
        self.r_area.setText(result['superficie'])
        for x in result['immagini']:
          tmp = QPushButton('Apri immagine')
          images.append(tmp)
          tmp.pressed.connect(lambda x=x: util.b64_display(x, self.conf))
          self.res_vbox.addWidget(tmp)
      except KeyError:
        self._flash(key, text='Il mappale {} non esiste!')
  
  def _flash(self, field, text='Campo "{}" non completato!', hide=False):
    self.message.setText(text.format(field))
    if hide:
      self.message.hide()
    else:
      self.message.show()
  
  def _write(self):
    tmp = {}
    # nome
    if not self.t_name.text():
      self._flash('nome')
      return
    else:
      tmp['nome'] = self.t_name.text()
    # cognome
    if not self.t_surname.text():
      self._flash('cognome')
      return
    else:
      tmp['cognome'] = self.t_surname.text()
    # comune
    if not self.t_city.text():
      self._flash('comune')
      return
    else:
      tmp['comune'] = self.t_city.text()
    # foglio
    if not self.t_paper.text():
      self._flash('foglio')
      return
    else:
      tmp['foglio'] = self.t_paper.text()
    # superficie
    if not self.t_area.text():
      self._flash('superficie')
      return
    else:
      tmp['superficie'] = self.t_area.text()
    # immagini
    if not self.images:
      self._flash('immagine', 'Nessuna {} caricata!')
      return
    else:
      tmp_list = []
      for x in self.images.values():
        tmp_list.append(x.decode())
      tmp['immagini'] = tmp_list
    # mappale
    key = self.t_map.text()
    if not key:
      self._flash('mappale')
      return
    elif key in self.db:
      self._flash(key, 'Il mappale {} esiste già!')
      return
    else:
      tmp['mappale'] = self.t_map.text()
    # cancella il messaggio
    self._flash('', '{}', True)
    # salva
    self.db[self.t_map.text()] = tmp
    util.save_db(self.conf, self.db)
    # resetta la pagina
    self.t_paper.setText('')
    self.t_map.setText('')
    self.t_area.setText('')
    for x in self.images.keys():
      self._delete(*x)
    self.images.clear()
  
  def _delete(self, *args):
    for x in args:
      x.hide()
  
  def exit(self):
    sys.exit(self.app.exec_())